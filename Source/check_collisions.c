/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2019 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include <math.h>
#include <stdlib.h>
#include <limits.h>
#include <stdio.h>

void check_collisions(Data* data)   /* Check collisions and manage memory */
{
    Planet *planet = (Planet *)data->planet;
    int number_Collisions = 0;
    int N_planets = data->N_planets;

    /* Loop variables are declared in the register to make code faster */
    register int n;
    register int l;
    /* This planet_copy is used as a temporary variable so that we can resize the planet array and free memory */
    Planet *planet_copy;

    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            switch ((int)(n > l))
            {
            case 1:
                if ((data->r[n][l] < 0.05*AU) && (n != l) && (planet[l].planet_id != 0 && planet[n].planet_id != 0)) /* Collisions occur if distance is less than 0.05AU - tweak this value if you wish! */
                {
                    number_Collisions++;
                    planet[l].planet_id = 0;
                    planet[n].vx = (planet[n].vx*planet[n].m + planet[l].vx*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].vy = (planet[n].vy*planet[n].m + planet[l].vy*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].vz = (planet[n].vz*planet[n].m + planet[l].vz*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].m = planet[n].m + planet[l].m;
                }
                break;
            case 0: // if (n < l) do the same thing but with r[l][n] instead
                if ( (data->r[l][n] < 0.05*AU) && (n != l) && (planet[l].planet_id != 0 && planet[n].planet_id != 0)) /* Collisions occur if distance is less than 0.05AU - tweak this value if you wish! */
                {
                    number_Collisions++;
                    planet[l].planet_id = 0;
                    planet[n].vx = (planet[n].vx*planet[n].m + planet[l].vx*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].vy = (planet[n].vy*planet[n].m + planet[l].vy*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].vz = (planet[n].vz*planet[n].m + planet[l].vz*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].m = planet[n].m + planet[l].m;
                }
                break;
            }
        }
    }

    if (number_Collisions != 0) /* Following code runs if there are collisions between planets */
    {
        size_t dummy = 0;

        for (n = 0; n < N_planets; n++)
        {
            if (planet[n].planet_id != 0)
            {
	            dummy++;
            }
        }

        planet_copy = (Planet*)malloc(dummy * sizeof(Planet));

        dummy = 0;

        for (n = 0; n < N_planets; n++)
        {
            if (planet[n].planet_id != 0)
            {
                planet_copy[dummy] = planet[n];
                dummy++;
            }
        }

        planet = (Planet*)realloc(planet,dummy * sizeof(Planet));
        data->memory_used -= (size_t)dummy * sizeof(Planet);

        if (dummy > INT_MAX) 
        {
            printf("Value exceeds int's maximum limit.\n");
            exit(EXIT_FAILURE);
        }

        for (n = 0; n < (int)dummy; n++)
        {
            planet[n] = planet_copy[n];
        }

        data->N_planets = (int)dummy;
        free(planet_copy);
        planet_copy = NULL;
    }
}

void check_collisions_2D(Data* data)   /* Check collisions and manage memory */
{
    Planet_2D *planet = (Planet_2D *)data->planet;
    int number_Collisions = 0;
    int N_planets = data->N_planets;

    /* Loop variables are declared in a register to make code faster */
    register int n;
    register int l;
    /* This planet_copy is used as a temporary variable so that we can resize the planet array and free memory */
    Planet_2D *planet_copy;

    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            switch ((int)(n > l))
            {
            case 1:
                if ((data->r[n][l] < 0.05*AU) && (n != l) && (planet[l].planet_id != 0 && planet[n].planet_id != 0)) /* Collisions occur if distance is less than 0.05AU - tweak this value if you wish! */
                {
                    number_Collisions++;
                    planet[l].planet_id = 0;
                    planet[n].vx = (planet[n].vx*planet[n].m + planet[l].vx*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].vy = (planet[n].vy*planet[n].m + planet[l].vy*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].m = planet[n].m + planet[l].m;
                }
                break;
            case 0: // if (n < l) do the same thing but with r[l][n] instead
                if ( (data->r[l][n] < 0.05*AU) && (n != l) && (planet[l].planet_id != 0 && planet[n].planet_id != 0)) /* Collisions occur if distance is less than 0.05AU - tweak this value if you wish! */
                {
                    number_Collisions++;
                    planet[l].planet_id = 0;
                    planet[n].vx = (planet[n].vx*planet[n].m + planet[l].vx*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].vy = (planet[n].vy*planet[n].m + planet[l].vy*planet[l].m) / (planet[n].m + planet[l].m);
                    planet[n].m = planet[n].m + planet[l].m;
                }
                break;
            }
        }
    }

    if (number_Collisions != 0) /* Following code runs if there are collisions between planets */
    {
        size_t dummy = 0;

        for (n = 0; n < N_planets; n++)
        {
            if (planet[n].planet_id != 0)
            {
	            dummy++;
            }
        }

        planet_copy = (Planet_2D*)malloc(dummy * sizeof(Planet_2D));

        dummy = 0;

        for (n = 0; n < N_planets; n++)
        {
            if (planet[n].planet_id != 0)
            {
                planet_copy[dummy] = planet[n];
                dummy++;
            }
        }

        planet = (Planet_2D*)realloc(planet,dummy * sizeof(Planet_2D));
        data->memory_used -= (size_t)dummy * sizeof(Planet_2D);

        if (dummy > INT_MAX) 
        {
            printf("Value exceeds int's maximum limit.\n");
            exit(EXIT_FAILURE);
        }

        for (n = 0; n < (int)dummy; n++)
        {
            planet[n] = planet_copy[n];
        }

        data->N_planets = (int)dummy;
        free(planet_copy);
        planet_copy = NULL;
    }
}
