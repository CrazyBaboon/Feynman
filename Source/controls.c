/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2019 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "types.h"
#include "update_motion.h"

#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int isDragging = 0; // Flag to track dragging state

// vallback for handling keyboard input
void process_keys(GLFWwindow *window, int key, int scancode, int action, int mods)
{
    Data* data = glfwGetWindowUserPointer(window);
    (void)mods; // silence compiler warning about unused parameter
    (void)scancode; // silence compiler warning

    if (action == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_ESCAPE : /* 'ESC' key */
            /* clean up the party before mom comes home */

            free(data->planet);
            data->planet = NULL;

            for(int n = 0; n < data->N_planets; n++)
            {
                free(data->r[n]);
                data->r[n] = NULL;
            }
            
            free(data->r);
            data->r = NULL;
            
            free(data);
            data = NULL;
            
            exit(0);
            
            break;

        case GLFW_KEY_P: /* 'p' key - enter Path mode */
            if (data->keyboard.P_key_pressed == 1)
            {
                data->keyboard.P_key_pressed = 0;
            }
            else
            {
                data->keyboard.P_key_pressed = 1;
            }
            break;

        case GLFW_KEY_O: /* 'o' key - enter Origin mode */
            
            if (data->keyboard.O_key_pressed < data->N_planets + 1)
            {
                data->keyboard.O_key_pressed++;
            }
            else
            {
                data->keyboard.O_key_pressed = 0;
            }
            break;

        case GLFW_KEY_I : /* 'i' key - zoom in */
            glClear(GL_COLOR_BUFFER_BIT);
            data->mouse.zoom_in_or_out = data->mouse.zoom_in_or_out / 1.5f;
            data->h = data->h / 2; /* Changes time step */
            break;

        case GLFW_KEY_U : /* 'u' key - zoom out */
            glClear(GL_COLOR_BUFFER_BIT);
            data->mouse.zoom_in_or_out = data->mouse.zoom_in_or_out*1.5f;
            data->h = data->h * 2; /* Changes time step */
            break;

        case GLFW_KEY_SPACE : /* 'spacebar' key - pause simulation */
            if (data->keyboard.Pause == 1)
            {
                data->keyboard.Pause = 0;
            }
            else
            {
                data->keyboard.Pause = 1;
            }
            break;
        }
    }
}

// Callback for handling mouse button input
// Callback for handling mouse button input
void process_mouse_buttons(GLFWwindow* window, int button, int state, int mods) 
{
    Data* data = glfwGetWindowUserPointer(window); // Retrieve user pointer

    (void)mods; // silence compiler warning
    double x, y;

    // Ensure we're handling the middle mouse button
    if (button == GLFW_MOUSE_BUTTON_MIDDLE) 
    {
        if (state == GLFW_PRESS) 
        {
            if (data->keyboard.P_key_pressed == 0) // Execute mouse translation only if not in 'fixed view' mode
            {
                glfwGetCursorPos(window, &x, &y); // Get cursor position at press
                data->mouse.xtemp = (int)x;
                data->mouse.ytemp = (int)y;

                isDragging = 1; // Start dragging
            } 
            else 
            {
                printf("Exit 'Fixed view mode' by pressing the 'o' key \n");
            }
        } 
        else if (state == GLFW_RELEASE) 
        {
            if (isDragging && data->keyboard.P_key_pressed == 0) // Only compute if dragging
            {
                glClear(GL_COLOR_BUFFER_BIT);

                glfwGetCursorPos(window, &x, &y); // Get cursor position at release
                data->mouse.move_right -= 0.02f * (float)(data->mouse.xtemp - x) 
                                                 * (float)AU * data->mouse.zoom_in_or_out;
                data->mouse.move_up += 0.02f * (float)(data->mouse.ytemp - y) * (float)AU * data->mouse.zoom_in_or_out;

                isDragging = 0; // Stop dragging
            }
        }
    }
}

// Scroll callback
void process_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset) 
{
    (void)xoffset; // silence compiler warning

    Data* data = glfwGetWindowUserPointer(window);

    if (yoffset > 0) { // Scroll up (zoom in)
        data->mouse.zoom_in_or_out /= 1.1f;
    } else if (yoffset < 0) { // Scroll down (zoom out)
        data->mouse.zoom_in_or_out *= 1.1f;
    }
}

void process_mouse_move(GLFWwindow* window, double x, double y) {

    Data* data = glfwGetWindowUserPointer(window); // Retrieve user pointer

    if (data->keyboard.O_key_pressed == 0 && isDragging)
    {
        data->mouse.move_right = data->mouse.move_right - 0.02f*(float)(data->mouse.xtemp-x)*(float)AU*data->mouse.zoom_in_or_out;
        data->mouse.move_up = data->mouse.move_up + 0.02f*(float)(data->mouse.ytemp-y)*(float)AU*data->mouse.zoom_in_or_out;
        data->mouse.xtemp = (int)x;
        data->mouse.ytemp = (int)y;
    }
}
