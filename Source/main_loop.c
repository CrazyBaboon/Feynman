/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2019 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include "auxiliary_calculations.h"
#include "update_motion.h"
#include "check_collisions.h"

#include <GLFW/glfw3.h>
#include <stdio.h>
#include <math.h>

Energy system_Energy;
Momentum system_Momentum;


// void main_loop(void)
// {
//     static int dummy_k = 0;

//     if (Keyboard.Pause == 0)
//     {
//         data->iteration++;
//         glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

//         if (data->iteration == 1)
//         {
//             glClear(GL_COLOR_BUFFER_BIT);
//         }

//         if (data->iteration-dummy_k == 200)
//         {
//             calculate_energies(&system_Energy);
//             calculate_momentum(&system_Momentum);
//             draw();
//             glFlush();
//             /* Output time (1 year = 31557600 seconds), number of celestial bodies and kinetic, potential, total energies and several linear and angular momenta */
//           //  printf("Time(Years)= %.4f, Number of CB's = %i,  K(J)= %.2E,  P(J)= %.2E, K+P(J)= %.2E\n",data->iteration*data->h/31557600, data->N_planets,system_Energy.KE,system_Energy.PE,system_Energy.KE+system_Energy.PE);
//           //  printf("LMx = %.2E, LMy = %.2E, LMz = %.2E, AMx = %.2E, AMy = %.2E, AMz = %.2E\n",system_Momentum.linear_x,system_Momentum.linear_y,system_Momentum.linear_z,system_Momentum.angular_x,system_Momentum.angular_y,system_Momentum.angular_z);
//           //  printf("\n");
//             dummy_k = data->iteration;

//             if (Keyboard.P_key_pressed == 0)
//             {
//                 glClear(GL_COLOR_BUFFER_BIT);
//             }
//         }

//         switch(NUMBER_OF_THREADS)
//         {
//         case 4:
//             update_motion_parallel();
//             break;
//         case 1:
//             update_motion();
//             break;
//         }
//         check_collisions();
//     }
//     glutPostRedisplay();
// }


void main_loop_2D(Data* data)
{
    static unsigned long long dummy_k = 0;

    if (data->keyboard.Pause == 0)
    {
        data->iteration++; 
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

        if (data->iteration == 1)
        {
            glClear(GL_COLOR_BUFFER_BIT);
        }

        if (data->iteration-dummy_k == 200)
        {
            calculate_energies_2D(&system_Energy, data);
            calculate_momentum_2D(&system_Momentum, data);

            /* Output time (1 year = 31557600 seconds), number of celestial bodies and kinetic, potential, total energies and several linear and angular momenta */
            printf("Time(Years)= %.4f, Number of CB's = %i,  K(J)= %.2E,  P(J)= %.2E, K+P(J)= %.2E\n",
                  (double)data->iteration*data->h/31557600, 
                  data->N_planets,
                  system_Energy.KE,
                  system_Energy.PE,
                  system_Energy.KE + system_Energy.PE);

            printf("LMx = %.2E, LMy = %.2E, AMz = %.2E\n",
                  system_Momentum.linear_x,
                  system_Momentum.linear_y,
                  system_Momentum.angular_z);

            printf("Memory used: %lu bytes\n",data->memory_used);

            printf("Iteration #%llu\n\n", data->iteration);
            printf("\n");
           
            dummy_k = data->iteration;

        }

        switch(NUMBER_OF_THREADS)
        {
        case 1:
            update_motion_2D(data);
            break;
        default:
            update_motion_parallel_2D(data);
        }
        check_collisions_2D(data);
    }
}



// void main_loop_3D(void)
// {
//     static int dummy_k = 0;

//     if (Keyboard.Pause == 0)
//     {
//         data->iteration++;
//         glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

//         if (data->iteration == 1)
//         {
//             glClear(GL_COLOR_BUFFER_BIT);
//         }

//         if (data->iteration - dummy_k == 500)
//         {
//             calculate_energies(&system_Energy);
//             calculate_momentum(&system_Momentum);
//             draw_3D();
//             glFlush();
//             /* Output time (1 year = 31557600 seconds), number of celestial bodies and kinetic, potential and total energies */
//             printf("Time(Years)= %.4f, Number of CB's = %i,  K(J)= %.2E,  P(J)= %.2E, K+P(J)= %.2E\n",data->iteration*data->h/31557600, data->N_planets,system_Energy.KE,system_Energy.PE,system_Energy.KE+system_Energy.PE);
//             printf("LMx = %.2E, LMy = %.2E, LMz = %.2E, AMx = %.2E, AMy = %.2E, AMz = %.2E\n",system_Momentum.linear_x,system_Momentum.linear_y,system_Momentum.linear_z,system_Momentum.angular_x,system_Momentum.angular_y,system_Momentum.angular_z);
//             printf("\n");

//             dummy_k = data->iteration;
//             if (Keyboard.P_key_pressed == 0)
//             {
//                 glClear(GL_COLOR_BUFFER_BIT);
//             }
//         }

//         switch(NUMBER_OF_THREADS)
//         {
//         case 4:
//             update_motion_parallel();
//             break;
//         case 1:
//             update_motion();
//             break;
//         }
//         check_collisions();
//     }
//     glutPostRedisplay();
// }
