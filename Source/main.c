/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2024 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
Any Feedback/Suggestions/Improvements greatly appreciated

This program implements the algorithm for N-Body gravitational attraction as suggested
by Richard Feynman on his book 'Lectures on Physics' Vol.I, chapter 9 section 7.

This computer program is licensed under the GNU General Public License version 3 or later

BWV 846
*/

#include "types.h"
#include "controls.h"
#include "main_loop.h"
#include "check_collisions.h"
#include "update_motion.h"
#include "input_data.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <GLFW/glfw3.h>
#include <time.h>

const double AU = 149.59E9;  /* Astronomical unit of distance */
const double G = 6.674E-11; /* Universal gravitation constant */

void draw_simulation(GLFWwindow   * window, 
                     Data            *data) 
{
    // Draw the line
    register int n;
    float x0;
    float y0;
    Planet_2D *planet = (Planet_2D *)data->planet;
    int N_planets = data->N_planets;

    for (n = 0; n < N_planets; n++)
    {
        if (data->keyboard.O_key_pressed == 0)
        {
            x0 = (float)((data->mouse.move_right + planet[n].x) / (data->mouse.zoom_in_or_out*AU*10)); /* the viewport does not move, what moves are the CB's */
            y0 = (float)((data->mouse.move_up    + planet[n].y) / (data->mouse.zoom_in_or_out*AU*10));
        }
        else
        {
            x0 = (float)((planet[n].x - planet[data->keyboard.O_key_pressed - 1].x) / (data->mouse.zoom_in_or_out*AU*10)); /* the view gets centered on the planet with index O_key_pressed */
            y0 = (float)((planet[n].y - planet[data->keyboard.O_key_pressed - 1].y) / (data->mouse.zoom_in_or_out*AU*10));
        }

        float a0 = 0.003f + (float)sqrt(planet[n].m / 3.285E23f) / 1500000.0f;   /* Radius of CB's is sqrt() of their mass - carry out some normalization*/
        float b0 = 0.002f + (float)sqrt(planet[n].m / 3.285E23f) / 2250000.0f;

        glBegin(GL_POLYGON);
        glColor3f(planet[n].color.red, planet[n].color.green, planet[n].color.blue); /* Color of the planet */
        glVertex2f( x0 + a0, y0);
        glVertex2f( x0 + b0, y0 + b0);
        glVertex2f( x0     , y0 + a0);
        glVertex2f( x0 - b0, y0 + b0);
        glVertex2f( x0 - a0, y0);
        glVertex2f( x0 - b0, y0 - b0);
        glVertex2f( x0     , y0 - a0);
        glVertex2f( x0 + b0, y0 - b0);
        glVertex2f( x0 + a0, y0);
        glEnd();
    }
    glfwSwapBuffers(window); // Equivalent to glutSwapBuffers
}

int main(void)
{
    Data* data;

    static unsigned long long dummy_k = 0;

    /* Import settings and input data from text files */
    data = malloc(sizeof(*data));

    /* Initiate keyboard control variables */
    data->keyboard.O_key_pressed = 0;
    data->keyboard.P_key_pressed = 0;
    data->keyboard.Pause = 0;

    /* Initiate Mouse control variables */
    data->mouse.zoom_in_or_out = 1;
    data->mouse.move_right = 0;
    data->mouse.move_up = 0;

    init_simulation(data);

    /* main loop of the program */
    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        return -1;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_SAMPLES, 0);  // Disable anti-aliasing (MSAA) if unnecessary


    GLFWwindow* window = glfwCreateWindow(600, 600, "Feynman 0.87", NULL, NULL);

    if (!window) {
        fprintf(stderr, "Failed to create GLFW window\n");
        glfwTerminate();
        return -1;
    }

    // input control data pointer for the window
    glfwSetWindowUserPointer(window, data);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);
    glfwSetKeyCallback(window, process_keys);
    glfwSetMouseButtonCallback(window, process_mouse_buttons);
    glfwSetScrollCallback(window, process_mouse_scroll);
    glfwSetCursorPosCallback(window, process_mouse_move);

    import_data_2D(data);
    //generate_data_2D(data);
    init_motion_2D(data);
    
    while (!glfwWindowShouldClose(window)) 
    {   
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        
        main_loop_2D(data);

        if (data->iteration-dummy_k == 200)
        {
            if (data->keyboard.P_key_pressed == 0)
            glClear(GL_COLOR_BUFFER_BIT);

            draw_simulation(window, data);
            
            dummy_k = data->iteration;
        }
        glfwPollEvents();        // Handles events (input, etc.)
    }

    glfwDestroyWindow(window);
    glfwTerminate();

    return 0;
}
