/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2019 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* EVERY 'EXTERN' VARIABLE DECLARED IN THIS FILE IS ACCESSIBLE TO ANY FILE THAT #include THIS FILE */

#ifndef _TYPES_H_
#define _TYPES_H_

#include <stddef.h>

#define DISPLAY(x) printf("A variavel "#x" tem o valor: %f\n", x);

#define NUMBER_OF_THREADS 1
/* #define GRAPHICS_3D */




/* Definition of all structures and Typedef's */
struct RGB{
    float red,green,blue;  /* this will describe the planet colour */
};

typedef struct PLANET{
    double x,y,z,vx,vy,vz,ax,ay,az,m;  /* Variables of motion distance, velocity, acceleration and mass */
    struct RGB color; /* Colour of the planet (I honestly prefer Color as it better matches the phonetics) */
    int planet_id;
} Planet;

/* two dimensional equivalent of "Planet". This is necessary to save memory when working only in 2D */
typedef struct PLANET_2D{
    double x,y,vx,vy,ax,ay,m;
    struct RGB color;
    int planet_id;
} Planet_2D;

/* Momentum structures that comprises linear and angular momentum */
typedef struct MOMENTUM{
    double linear_x; //Linear Momentum of the system in the x-direction
    double linear_y; //Linear Momentum of the system in the y-direction
    double linear_z; //Linear Momentum of the system in the z-direction
    double angular_x; //Angular Momentum of the system
    double angular_y; //Angular Momentum of the system
    double angular_z; //Angular Momentum of the system
} Momentum;

typedef struct MOMENTUM_2D{
    double linear_x; //Linear Momentum of the system in the x-direction
    double linear_y; //Linear Momentum of the system in the y-direction
} Momentum_2D;

/* Energy structure that comprises potential and kinetic energy */
typedef struct ENERGY{
    double KE; //Kinetic energy of the system
    double PE; //Kinetic energy of the system
} Energy;

/* Structure that comprises keyboard control flags */
typedef struct KEYBOARD_CONTROL{
    short int P_key_pressed;
    short int O_key_pressed;
    short int Pause;
} Keyboard_control;

/* Structure that comprises mouse control flags */
typedef struct MOUSE_CONTROL{
    float zoom_in_or_out;
    float move_right;
    float move_up;
    int xtemp;
    int ytemp;
} Mouse_control;

typedef enum {
    simulation_2D,
    simulation_3D,
}simulationType;

typedef struct DATA{
    void *planet;
    Momentum system_Momentum;
    Energy system_Energy;
    double **r; /* matrix of the distance between CB's. This matrix is triangular so in the code you will find
        lots of tricks involving r[n][l] and r[l][n], just to save up a lot of memory. The extra spaggetti is worth it!
        Alternatively one could re-write the program without the matrix r being declared at all,
        but that would require r to be calculated multiple times and therefore would make the code slower
        (when many CB's are considered) than if r is only calculated once!*/
    int N_planets; /* number of celestial bodies (CB's) */
    unsigned long long iteration; /* variable used to set the time marching algorithm */
    double h; /* Time resolution in seconds */
    size_t memory_used;
    simulationType simulation_type;
    Keyboard_control keyboard;
    Mouse_control mouse;
}Data;


/* Here are all the global variables used in Feynman */
extern int N_planets;    /* number of celestial bodies (CB's) */
extern const double AU;  /* Astronomical unit of distance */
extern const double G;   /* Universal gravitation constant */
extern Data *data;

#endif
