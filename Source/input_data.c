/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2024 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
input_data.c - This file reads the input data files found in the 'input_data/run'
folder.

The function "input_data_reader()". This function does five
operations:

1. Sets 'time_resolution' from "input_data/run/settings.csv"

2. Sets the number of planets in the text files by counting the number of lines
in "input_data/run/positions.csv". It also reserves memory for these planets and
corresponding distance matrix.

3. Sets the initial positions of each of the planets from "input_data/run/positions.csv"

4. Sets the initial velocities of each of the planets from "input_data/run/velocities.csv"

5. Sets the initial masses of each of the planets from "input_data/run/mass.csv".
*/

#include "types.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

extern Energy system_Energy;
extern Momentum system_Momentum;


void init_simulation(Data *data)
{
  srand((unsigned int)time(NULL));

  data->iteration = 0;
  data->memory_used = 0;
  
  /* Decide from the text file if a 2D or 3D simulation is required */
  
  FILE *fp = fopen("input_data/run/positions.csv", "r");
 
  if (fp == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  char buffer[255];
  int ch;
  unsigned char dimension_tmp = 0;

  if (fgets(buffer, sizeof buffer, fp) != NULL) /* test if header of the file exists */
  {
     while ((ch = fgetc(fp)) != EOF) /* read character by character */
     {
         if (ch == '\n' || ch == '\0') /* if a new line is found, exit */
         {
             break;
         }
         else if (ch == ' ') /* check for white spaces */
         {
		     dimension_tmp++;
	     }
     }
  }

  data->simulation_type = dimension_tmp - 1;

  fclose(fp);


  /* Initiate Energy and Momentum variables */
  data->system_Energy.KE = 0;
  data->system_Energy.PE = 0;
  data->system_Momentum.linear_x = 0;
  data->system_Momentum.linear_y = 0;
  data->system_Momentum.linear_z = 0;
  system_Energy.KE = 0;
  system_Energy.PE = 0;
  system_Momentum.linear_x = 0;
  system_Momentum.linear_y = 0;
  system_Momentum.linear_z = 0;

  /* Initiate keyboard control variables */
  data->keyboard.O_key_pressed = 0;
  data->keyboard.P_key_pressed = 0;
  data->keyboard.Pause = 0;

  /* Initiate Mouse control variables */
  data->mouse.zoom_in_or_out = 1;
  data->mouse.move_right = 0;
  data->mouse.move_up = 0;
}



void import_data(Data *data)
{
  FILE *fp = fopen("input_data/run/time_resolution.csv", "r");

  int n; /* dummy loop variable */

  if (fp == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  char buffer[255];

  /* Reads time resolution (double h) from the file: */
  float time_resolution;

  if (fscanf(fp, "%f", &time_resolution) != 1)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }
  data->h = time_resolution;

  fclose(fp);

  FILE *fp2 = fopen("input_data/run/positions.csv", "r");

  if (fp2 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  /* reads how many planets there are by counting the number of position coordinates. */
  data->N_planets = 0;

  while (fgets(buffer, sizeof buffer, fp2) != NULL)
  {
      data->N_planets++;
  }
  int N_planets = data->N_planets;

  data->planet = malloc((size_t)N_planets * sizeof(Planet));
  data->memory_used += (size_t)N_planets * sizeof(Planet);
  Planet *planet = (Planet *)data->planet;

  /* create distance matrix r dynamically: */
  data->r = (double **) malloc((size_t)N_planets * sizeof(double*));
  data->memory_used += (size_t)N_planets * sizeof(double*);

  for(n = 0; n < N_planets; n++)
  {
      /* r is a triangular matrix (each row n is assigned n columns), saving twice as much memory compared to a square matrix */
      data->r[n] = (double *) malloc((size_t)(n+1)*sizeof(double));
      data->memory_used += (size_t)(n+1)*sizeof(double);
  }

  fclose(fp2);

  /* reads the coordinates of the Celestial Bodies: */
  FILE *fp3 = fopen("input_data/run/positions.csv", "r");

  if (fp3 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  float x_tmp;
  float y_tmp;
  float z_tmp;

  for (n = 0; n < N_planets; n++)
  {
      fscanf(fp3, "%f,%f,%f", &x_tmp, &y_tmp, &z_tmp);
      planet[n].x = x_tmp;
      planet[n].y = y_tmp;
      planet[n].z = z_tmp;
  }

  fclose(fp3);

  /* reads the velocities of the Celestial Bodies: */
  FILE *fp4 = fopen("input_data/run/velocities.csv", "r");

  if (fp4 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  float vx_tmp;
  float vy_tmp;
  float vz_tmp;

  for (n = 0; n < N_planets; n++)
  {
      fscanf(fp4, "%f,%f,%f", &vx_tmp, &vy_tmp, &vz_tmp);
      planet[n].vx = vx_tmp;
      planet[n].vy = vy_tmp;
      planet[n].vz = vz_tmp;
  }

  fclose(fp4);

  /* reads the masses of the Celestial Bodies: */
  FILE *fp5 = fopen("input_data/run/mass.csv", "r");

  if (fp5 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  float mass_tmp;

  for (n = 0; n < N_planets; n++)
  {
      fscanf(fp5, "%f", &mass_tmp);
      planet[n].m = mass_tmp;
  }
  fclose(fp5);

  /* Define the rest of the data fields of the planets */
  for (n = 0; n < N_planets; n++)
  {
      planet[n].planet_id = n+1;
      planet[n].ax = 0;
      planet[n].ay = 0;
      planet[n].az = 0;
      planet[n].color.red =   (float)(10*(rand() % 2) + 0.4); /* Set random planet colours */
      planet[n].color.green = (float)(10*(rand() % 2) + 0.4);
      planet[n].color.blue =  (float)(10*(rand() % 2) + 0.4);
  }
}



void import_data_2D(Data *data)
{
  FILE *fp = fopen("input_data/run/time_resolution.csv", "r");

  int n; /* dummy loop variable */

  if (fp == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  char buffer[255];

  /* Reads time resolution (double h) from the file: */
  float time_resolution;

  if (fscanf(fp, "%f", &time_resolution) != 1)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }
  data->h = time_resolution;

  fclose(fp);

  FILE *fp2 = fopen("input_data/run/positions.csv", "r");

  if (fp2 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  /* reads how many planets there are by counting the number of position coordinates. */
  data->N_planets = 0;

  while (fgets(buffer, sizeof buffer, fp2) != NULL)
  {
      data->N_planets++;
  }

  int N_planets = data->N_planets;

  data->planet = malloc((size_t)N_planets * sizeof(Planet_2D));
  data->memory_used += (size_t)N_planets * sizeof(Planet_2D);
  Planet_2D *planet = (Planet_2D *)data->planet;

  /* create distance matrix r dynamically: */
  data->r = (double **) malloc((size_t)N_planets * sizeof(double*));
  data->memory_used += (size_t)N_planets * sizeof(double*);
  
  for(n = 0; n < N_planets; n++)
  {
      /* r is a triangular matrix (each row n is assigned n columns), saving twice as much memory compared to a square matrix */
      data->r[n] = (double *) malloc((size_t)(n+1)*sizeof(double));
      data->memory_used += (size_t)(n+1)*sizeof(double);
  }

  fclose(fp2);

  /* reads the coordinates of the Celestial Bodies: */
  FILE *fp3 = fopen("input_data/run/positions.csv", "r");

  if (fp3 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  float x_tmp;
  float y_tmp;

  for (n = 0; n < N_planets; n++)
  {
      fscanf(fp3, "%f,%f", &x_tmp, &y_tmp);
      planet[n].x = x_tmp * AU;
      planet[n].y = y_tmp * AU;
  }

  fclose(fp3);

  /* reads the velocities of the Celestial Bodies: */
  FILE *fp4 = fopen("input_data/run/velocities.csv", "r");

  if (fp4 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  float vx_tmp;
  float vy_tmp;

  for (n = 0; n < N_planets; n++)
  {
      fscanf(fp4, "%f,%f", &vx_tmp, &vy_tmp);
      planet[n].vx = vx_tmp;
      planet[n].vy = vy_tmp;
  }

  fclose(fp4);

  /* reads the masses of the Celestial Bodies: */
  FILE *fp5 = fopen("input_data/run/mass.csv", "r");

  if (fp5 == NULL)
  {
      perror("fopen");
      exit(EXIT_FAILURE);
  }

  float mass_tmp;

  for (n = 0; n < N_planets; n++)
  {
      fscanf(fp5, "%f", &mass_tmp);
      planet[n].m = mass_tmp;
  }
  fclose(fp5);

  /* Define the rest of the data fields of the planets */
  for (n = 0; n < N_planets; n++)
  {
      planet[n].planet_id = n+1;
      planet[n].ax = 0;
      planet[n].ay = 0;
      planet[n].color.red =   (float)(10*(rand() % 2) + 0.4); /* Set random planet colours */
      planet[n].color.green = (float)(10*(rand() % 2) + 0.4);
      planet[n].color.blue =  (float)(10*(rand() % 2) + 0.4);
  }
}

void generate_data_2D(Data *data)
{
    data->h = 1200;

    data->N_planets = 100;
    int N_planets = data->N_planets;

    data->planet = malloc((size_t)N_planets * sizeof(Planet_2D));
    data->memory_used += (size_t)N_planets * sizeof(Planet_2D);
    Planet_2D *planet = (Planet_2D *)data->planet;

    /* create distance matrix r dynamically: */
    data->r = (double **) malloc((size_t)N_planets * sizeof(double*));
    data->memory_used += (size_t)N_planets * sizeof(double*);
  
    for(int n = 0; n < N_planets; n++)
    {
        /* r is a triangular matrix (each row n is assigned n columns), saving twice as much memory compared to a square matrix */
        data->r[n] = (double *) malloc((size_t)(n+1)*sizeof(double));
        data->memory_used += (size_t)(n+1)*sizeof(double);
    } 

    for (int n=0; n<N_planets; n++)
    {
        planet[n].planet_id=n+1;
        planet[n].x=AU*15*(((double)rand())/RAND_MAX-0.5);  
        planet[n].y=AU*15*(((double)rand())/RAND_MAX-0.5); 
        planet[n].vx=10000*(((double)rand())/RAND_MAX-0.5); 
        planet[n].vy=10000*(((double)rand())/RAND_MAX-0.5); 
        planet[n].ax=0;
        planet[n].ay=0;	
        planet[n].m=1E29; 
        planet[n].color.red=(float)(rand() % 2)*10+0.4f; //Set random planet colours
        planet[n].color.green=(float)(rand() % 2)*10+0.4f;  
        planet[n].color.blue=(float)(rand() % 2)*10+0.4f;     
    } 
}
