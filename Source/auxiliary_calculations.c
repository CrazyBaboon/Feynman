/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2019 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include <math.h>

void calculate_energies(Energy *e, Data* data)
{
    Planet *planet = (Planet *)data->planet;
    int N_planets = data->N_planets;
    double KE = 0;
    double PE = 0;

    register int n;
    register int l;

    for (n = 0; n < N_planets; n++)
    {
        KE = (KE + 0.5*planet[n].m*(pow(planet[n].vx,2) + pow(planet[n].vy,2) + pow(planet[n].vz,2))  );  /* Calculate Kinetic Energy of the system of particles   K = 0.5*m*(vx^2+vy^2) */
    }

    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if(n > l)
            {
                PE = PE - G*(planet[l].m)*(planet[n].m) / (data->r[n][l]); /* Calculate potential energy of the system   P=-G*m1*m2/r */
            }
        }
    }

    e->KE = KE;
    e->PE = PE;
}




void calculate_energies_2D(Energy *e, Data* data)
{
    Planet_2D *planet = (Planet_2D *)data->planet;
    int N_planets = data->N_planets;
    double KE = 0;
    double PE = 0;

    register int n;
    register int l;

    for (n = 0; n < N_planets; n++)
    {
        KE = (KE + 0.5*planet[n].m*(pow(planet[n].vx,2) + pow(planet[n].vy,2) )  );  /* Calculate Kinetic Energy of the system of particles   K = 0.5*m*(vx^2+vy^2) */
    }

    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if(n > l)
            {
                PE = PE - G*(planet[l].m)*(planet[n].m) / (data->r[n][l]); /* Calculate potential energy of the system   P=-G*m1*m2/r */
            }
        }
    }

    e->KE = KE;
    e->PE = PE;
}



int calculate_momentum(Momentum *m, Data* data)
{
    Planet *planet = (Planet *)data->planet;
    int N_planets = data->N_planets;
    double linear_x = 0;
    double linear_y = 0;
    double linear_z = 0;
    double angular_x = 0;
    double angular_y = 0;
    double angular_z = 0;

    register int n;

/* Calculate linear momentum of particles */

    for (n=0; n<N_planets; n++)
    {
        linear_x = linear_x + planet[n].m * planet[n].vx;
        linear_y = linear_y + planet[n].m * planet[n].vy;
        linear_z = linear_z + planet[n].m * planet[n].vz;
    }

    m->linear_x = linear_x;
    m->linear_y = linear_y;
    m->linear_z = linear_z;

/* Calculate angular momentum of particles */


    for (n=0; n<N_planets; n++)
    {
        angular_x = angular_x + planet[n].m * (planet[n].y * planet[n].vz - planet[n].z * planet[n].vy);
        angular_y = angular_y + planet[n].m * (planet[n].z * planet[n].vx - planet[n].x * planet[n].vz);
        angular_z = angular_z + planet[n].m * (planet[n].x * planet[n].vy - planet[n].y * planet[n].vx);
    }



    m->angular_x = angular_x;
    m->angular_y = angular_y;
    m->angular_z = angular_z;

    return 0;
}

int calculate_momentum_2D(Momentum *m, Data* data)
{
    Planet_2D *planet = (Planet_2D *)data->planet;
    int N_planets = data->N_planets;
    
    double linear_x = 0;
    double linear_y = 0;

    double angular_z = 0;

    register int n;

/* Calculate linear momentum of particles */

    for (n=0; n<N_planets; n++)
    {
        linear_x = linear_x + planet[n].m * planet[n].vx;
        linear_y = linear_y + planet[n].m * planet[n].vy;
    }

    m->linear_x = linear_x;
    m->linear_y = linear_y;

/* Calculate angular momentum of particles */


    for (n=0; n<N_planets; n++)
    {
        angular_z = angular_z + planet[n].m * (planet[n].x * planet[n].vy - planet[n].y * planet[n].vx);
    }

    m->angular_z = angular_z;

    return 0;
}



