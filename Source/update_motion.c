/* Feynman  - A simple astronomy simulator written in the C programming language
 * Copyright (C) 2016-2018 Nuno Ferreira
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "types.h"
#include <assert.h>
#include <math.h>
#include <omp.h>

// pthread_t threads[NUMBER_OF_THREADS];
// int thread_args[NUMBER_OF_THREADS];



void init_motion(Data *data)
{
/* As indicated in section 9-7 of Feynman Lectures in Physics - Volume I, the first motion
iteration gets a factor of 0.5 multiplied by the velocities. This is the only difference from
the remainder of the motion update equations. */

    int N_planets = data->N_planets;
    double h = data->h; /* time step */
    int n, l; /* dummy loop variables */

    Planet *planet    = (Planet *)    data->planet;
    /* Calculate distances between CB's */
    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n > l)
            {
                data->r[n][l] = sqrt(pow((planet[n].x-planet[l].x),2) 
                                   + pow((planet[n].y-planet[l].y),2) 
                                   + pow((planet[n].z-planet[l].z),2) ) ;
            }
            else
            {
                continue;
            }
        }
    }

    /* Calculate accelerations of the CB's: */
    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n == l)
            {
	        planet[n].ax = planet[n].ax;
                planet[n].ay = planet[n].ay;
                planet[n].az = planet[n].az;
            }
            else if (n > l)
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[n][l],3);
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[n][l],3);
                planet[n].az = planet[n].az+ -G*(planet[l].m)*(planet[n].z-planet[l].z) / pow(data->r[n][l],3);
            }
            else
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[l][n],3);
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[l][n],3);
                planet[n].az = planet[n].az+ -G*(planet[l].m)*(planet[n].z-planet[l].z) / pow(data->r[l][n],3);
            }
        }
    }

    /* Calculate velocities of the CB's: */
    for (n = 0; n < N_planets; n++)
    {
        planet[n].vx = planet[n].vx + planet[n].ax*h*0.5;
        planet[n].vy = planet[n].vy + planet[n].ay*h*0.5;
        planet[n].vz = planet[n].vz + planet[n].az*h*0.5;
    }
}


void init_motion_2D(Data *data)
{
/* As indicated in section 9-7 of Feynman Lectures in Physics - Volume I, the first motion
iteration gets a factor of 0.5 multiplied by the velocities. This is the only difference from
the remainder of the motion update equations. */
    
    int N_planets = data->N_planets;
    double h = data->h; /* time step */
    int n, l; /* dummy loop variables */

    Planet_2D *planet = (Planet_2D *)data->planet;
    
    /* Calculate distances between CB's */
    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n > l)
            {
                data->r[n][l] = sqrt(pow((planet[n].x-planet[l].x),2) + pow((planet[n].y-planet[l].y),2) ) ;
            }
            else
            {
                continue;
            }
        }
    }

    /* Calculate accelerations of the CB's: */
    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n == l)
            {
	            planet[n].ax = planet[n].ax;
                planet[n].ay = planet[n].ay;
            }
            else if (n > l)
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[n][l],3);
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[n][l],3);
            }
            else
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[l][n],3);
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[l][n],3);
            }
        }
    }

    /* Calculate velocities of the CB's: */
    for (n = 0; n < N_planets; n++)
    {
        planet[n].vx = planet[n].vx + planet[n].ax*h*0.5;
        planet[n].vy = planet[n].vy + planet[n].ay*h*0.5;
    }
}





/* Update motion using single core computation */
void update_motion(Data *data)
{
    double h = data->h;
    /* Loop variables are declared in the register to make code faster */
    register int n;
    register int l;
    Planet *planet    = (Planet *)    data->planet;
    int N_planets = data->N_planets;
    
    for (n = 0; n < N_planets; n++)
    {
        planet[n].x = planet[n].x + planet[n].vx*h;  /* Update positions of the CB's */
        planet[n].y = planet[n].y + planet[n].vy*h;
        planet[n].z = planet[n].z + planet[n].vz*h;
    }

    /*   Calculate distances between CB's   */
    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n > l)
            {
            data->r[n][l] = sqrt(pow((planet[n].x - planet[l].x),2) + pow((planet[n].y - planet[l].y),2) + pow((planet[n].z - planet[l].z),2));
            }
            else
            {
            continue;  /* continue statement is necessary because data->r[n][l] is triangular and therefore not defined when n<=l */
            }
        }
    }

	/*   Calculate accelerations of the CB's   */
    for (n = 0; n < N_planets; n++)
    {
        planet[n].ax = 0;  	/* Set accelerations to 0 */
        planet[n].ay = 0;
        planet[n].az = 0;
        for (l = 0; l < N_planets; l++)
        {
            if (n == l)
            {
                planet[n].ax = planet[n].ax;
                planet[n].ay = planet[n].ay;
                planet[n].az = planet[n].az;
            }
            else if (n > l)
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[n][l],3)   ;
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[n][l],3)   ;
                planet[n].az = planet[n].az+ -G*(planet[l].m)*(planet[n].z-planet[l].z) / pow(data->r[n][l],3)   ;
            }
            else
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[l][n],3)   ;
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[l][n],3)   ;
                planet[n].az = planet[n].az+ -G*(planet[l].m)*(planet[n].z-planet[l].z) / pow(data->r[l][n],3)   ;
            }
        }
    }

    /*   Calculate velocities of the CB's   */
    for (n = 0; n < N_planets; n++)
    {
        planet[n].vx = planet[n].vx + planet[n].ax*h;
        planet[n].vy = planet[n].vy + planet[n].ay*h;
        planet[n].vz = planet[n].vz + planet[n].az*h;
    }
}


/* Update motion using single core computation */
void update_motion_2D(Data *data)
{
    double h = data->h;
    /* Loop variables are declared in the register to make code faster */
    register int n;
    register int l;
    Planet_2D *planet    = (Planet_2D *)data->planet;
    int N_planets = data->N_planets;
    
    for (n = 0; n < N_planets; n++)
    {
        planet[n].x = planet[n].x + planet[n].vx*h;  /* Update positions of the CB's */
        planet[n].y = planet[n].y + planet[n].vy*h;
    }


    /*   Calculate distances between CB's   */

    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n > l)
            {
            data->r[n][l] = sqrt(pow((planet[n].x - planet[l].x),2) + pow((planet[n].y - planet[l].y),2) );
            }
            else
            {
            continue;  /* continue statement is necessary because data->r[n][l] is triangular and therefore not defined when n<=l */
            }
        }
    }

	/*   Calculate accelerations of the CB's   */
    for (n = 0; n < N_planets; n++)
    {
        planet[n].ax = 0;  	/* Set accelerations to 0 */
        planet[n].ay = 0;
        for (l = 0; l < N_planets; l++)
        {
            if (n == l)
            {
                planet[n].ax = planet[n].ax;
                planet[n].ay = planet[n].ay;
            }
            else if (n > l)
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[n][l],3)   ;
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[n][l],3)   ;
            }
            else
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[l][n],3)   ;
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[l][n],3)   ;
            }
        }
    }

    /*   Calculate velocities of the CB's   */
    for (n = 0; n < N_planets; n++)
    {
        planet[n].vx = planet[n].vx + planet[n].ax*h;
        planet[n].vy = planet[n].vy + planet[n].ay*h;
    }
}







/* The 'update_accelerations' function is called by the multithread 'update_motion_parallel' function */
void* update_accelerations(void* argument, Data *data)
{
    int thread_number;
    thread_number = *((int *) argument);
    /* indexes of planets to be considered in the nth thread: */
    Planet *planet    = (Planet *)data->planet;
    int N_planets = data->N_planets;
    
    int N_planets_thread[8]={0,N_planets/4,N_planets/4+1,2*N_planets/4,2*N_planets/4+1,3*N_planets/4,3*N_planets/4+1,N_planets-1};

    /* Loop variables are declared in the register to make code faster */
    register int n;
    register int l;


    /* Calculate accelerations of the CB's */
    for (n = N_planets_thread[2*thread_number]; n <= N_planets_thread[2*thread_number+1]; n++)  /* divide the planets per thread */
    {
        planet[n].ax = 0;  	/* Set accelerations to 0 */
        planet[n].ay = 0;
        planet[n].az = 0;

	for (l = 0; l < N_planets; l++)
        {
            if(n == l)
            {
                planet[n].ax = planet[n].ax;
                planet[n].ay = planet[n].ay;
                planet[n].az = planet[n].az;
            }
            else if (n > l)
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[n][l],3);
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[n][l],3);
                planet[n].az = planet[n].az+ -G*(planet[l].m)*(planet[n].z-planet[l].z) / pow(data->r[n][l],3);}
            else
            {
                planet[n].ax = planet[n].ax+ -G*(planet[l].m)*(planet[n].x-planet[l].x) / pow(data->r[l][n],3);
                planet[n].ay = planet[n].ay+ -G*(planet[l].m)*(planet[n].y-planet[l].y) / pow(data->r[l][n],3);
                planet[n].az = planet[n].az+ -G*(planet[l].m)*(planet[n].z-planet[l].z) / pow(data->r[l][n],3);
            }
        }
    }
    return NULL;
}



/* The 'update_distance_matrix' function is called by the multithread 'update_motion_parallel' function */
void* update_distance_matrix(void* argument, Data *data)
{
    /* Loop variables are declared in the register to make code faster */
    register int n;
    register int l;
    Planet *planet = (Planet *)data->planet;
    int N_planets = data->N_planets;
    (void)argument; /* unused but required for thread entry point */

    /* Calculate distances between CB's: */

    for (n = 0; n < N_planets; n++)
    {
        for (l = 0; l < N_planets; l++)
        {
            if (n > l)
            {
                data->r[n][l] = sqrt(pow((planet[n].x-planet[l].x),2) + pow((planet[n].y-planet[l].y),2) + pow((planet[n].z-planet[l].z),2));
            }
            else
            {
                continue;
            }  /* continue statement is necessary because r[n][l] is triangular and therefore not defined when n<=l */
        }
    }
    return NULL;
}


/* Update motion if multithread is selected (#define NUMBER_OF_THREADS in main.h) */
void update_motion_parallel(Data *data)
{
    double h = data->h;
    Planet *planet = (Planet *)data->planet;
    int N_planets = data->N_planets;

    /* Update positions of the CB's */
    #pragma omp parallel for num_threads(NUMBER_OF_THREADS) schedule(static)
    for (int n = 0; n < N_planets; n++)
    {
        planet[n].x += planet[n].vx * h;
        planet[n].y += planet[n].vy * h;
        planet[n].z += planet[n].vz * h;
    }

    /* Calculate CB's distance matrix */
    #pragma omp parallel num_threads(NUMBER_OF_THREADS)
    {
        int thread_id = omp_get_thread_num();
        update_distance_matrix(&thread_id, data);
    }

    /* Calculate CB's accelerations */
    #pragma omp parallel num_threads(NUMBER_OF_THREADS)
    {
        int thread_id = omp_get_thread_num();
        update_accelerations(&thread_id, data);
    }

    /* Calculate velocities of the CB's */
    #pragma omp parallel for num_threads(NUMBER_OF_THREADS) schedule(static)
    for (int n = 0; n < N_planets; n++)
    {
        planet[n].vx += planet[n].ax * h;
        planet[n].vy += planet[n].ay * h;
        planet[n].vz += planet[n].az * h;
    }
}


/* Update motion if multithread is selected (#define NUMBER_OF_THREADS in main.h) */
void update_motion_parallel_2D(Data *data)
{
    double h = data->h;
    Planet_2D *planet = (Planet_2D *)data->planet;
    int N_planets = data->N_planets;
    
    /* Update positions of the CB's */
    #pragma omp parallel for num_threads(NUMBER_OF_THREADS)
    for (int n = 0; n < N_planets; n++)
    {
        planet[n].x = planet[n].x + planet[n].vx * h;
        planet[n].y = planet[n].y + planet[n].vy * h;
    }

    /* Calculate CB's distance matrix in parallel */
    #pragma omp parallel num_threads(NUMBER_OF_THREADS)
    {
    /* Calculate distances between CB's: */

    for (int n = 0; n < N_planets; n++)
    {
        for (int l = 0; l < N_planets; l++)
        {
            if (n > l)
            {
                data->r[n][l] = sqrt(pow((planet[n].x-planet[l].x),2) + pow((planet[n].y-planet[l].y),2));
            }
            else
            {
                continue;
            }  /* continue statement is necessary because r[n][l] is triangular and therefore not defined when n<=l */
        }
    }
    }

    /* Calculate CB's accelerations in parallel */
    #pragma omp parallel num_threads(NUMBER_OF_THREADS)
    {
    int thread_id = omp_get_thread_num();
    
    // Calculate dynamic thread chunk of work as opposed to the previous fixed array assignment.
    int threads_total = omp_get_num_threads(); // Total number of threads
    int chunk_size = (N_planets + threads_total - 1) / threads_total; // Round up division
    int start = thread_id * chunk_size; // Start index for this thread
    int end = (start + chunk_size < N_planets) ? (start + chunk_size) : N_planets; // End index

    // Loop variables
    register int n, l;

    /* Calculate accelerations of the CB's */
    for (n = start; n < end; n++) {
        planet[n].ax = 0;
        planet[n].ay = 0;

        for (l = 0; l < N_planets; l++) {
            if (n > l) {
                planet[n].ax += -G * planet[l].m * (planet[n].x - planet[l].x) / pow(data->r[n][l], 3);
                planet[n].ay += -G * planet[l].m * (planet[n].y - planet[l].y) / pow(data->r[n][l], 3);
            } 
        }
    }
    }

    /* Calculate velocities of the CB's */
    #pragma omp parallel for num_threads(NUMBER_OF_THREADS)
    for (int n = 0; n < N_planets; n++)
    {
        planet[n].vx = planet[n].vx + planet[n].ax * h;
        planet[n].vy = planet[n].vy + planet[n].ay * h;
    }
}
