# Feynman

A powerful n-body simulator.

(c) 2016-2024 Nuno Ferreira and contributors

GNU GPL version 3 or later


## Building

Install dependencies on Linux (Debian-like) systems:

    sudo apt install clang git make libglfw3 libglfw3-dev libomp-dev

To compile type:

    make

To run type:

    ./feynman

## Controls & Shortcuts

 * **Mouse wheel** - zoom in, zoom out (zooms in to whatever is at the centre of the screen)
 * **Click/drag/unclick** - move around the scene

Keyboard:

 * **Esc** - Quit Feynman
 * **i**  - Time-scaled Zoom in
 * **u**  - Time-scaled Zoom out
 * **o**  - Set view origin to be fixed in a planet
 * **p**  - Toggle 'path view' mode
 * **Spacebar** - Pause

Run using the terminal to have access to the potential and kinetic energy data.

## Contributors

Sijmen J. Mulder

