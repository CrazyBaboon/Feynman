CC      = clang
TARGET  = feynman
SOURCES = Source/*.c
HEADERS = Source/headers/*.h

# For debug build
CFLAGS  += -g

CFLAGS += -std=c99
CFLAGS += -Wall -Wextra -Wpedantic -Wconversion

# Include HEADERS
CFLAGS += -ISource/headers

# Tweak to match platform
CFLAGS  += -I/usr/local/include
CFLAGS  += -I/usr/X11R6/include

LDFLAGS += -fopenmp
LDFLAGS += -L/usr/local/lib
LDFLAGS += -L/usr/X11R6/lib

LDLIBS  += -lglfw -lGL -lm


all: $(TARGET)

$(TARGET): $(SOURCES) $(HEADERS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(SOURCES) $(LDLIBS)

clean:
	rm -f feynman

.PHONY: all clean
